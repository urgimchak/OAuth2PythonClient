from bottle import request, get, post, route, run, template
import psycopg2
import hashlib


@route('/reg')
def reg():
    return '''
        <form action="/reg" method="post">
            Username: <input name="username" type="text" />
            Password: <input name="password" type="password" />
            <input value="Register" type="submit" />
        </form>
    '''


@post('/reg')
def do_reg():
    try:
        conn = psycopg2.connect("dbname='criptoservice' user='postgres' host='localhost' password='123'")
    except:
        print("I am unable to connect to the database")
    username = request.forms.get('username')
    password = request.forms.get('password')
    cursor = conn.cursor()
    try:
        hashpassword=hashlib.md5(password.encode('utf-8')).hexdigest()
        sql = """INSERT INTO users(username,password) VALUES(%s,%s) RETURNING id;"""
        cursor.execute(sql, (username, hashpassword,))
        conn.commit()
        cursor.close()
        return '''Вы успешно зарегестрированы'''
    except Exception as e:
        print(e)
        cursor.close()
        return "Возможно логин уже занят кем-то другим. Попробуйте ещё"


@route('/login')
def login():
    return '''
        <form action="/login" method="post">
            Username: <input name="username" type="text" />
            Password: <input name="password" type="password" />
            <input value="Login" type="submit" />
        </form>
    '''


@post('/login')
def do_login():
    try:
        conn = psycopg2.connect("dbname='criptoservice' user='postgres' host='localhost' password='123'")
    except Exception as e:
        return" I am unable to connect to the database"
    username = request.forms.get('username')
    password = request.forms.get('password')
    cursor = conn.cursor()
    if check_username_and_password(username, password, cursor):
        return True
    else:
        return False


def check_username_and_password(username, password, cursor):
    hashpassword = hashlib.md5(password.encode('utf-8')).hexdigest()
    sql = """SELECT * FROM users WHERE username=%s and password=%s;"""
    cursor.execute(sql, (username, hashpassword,))
    if cursor.rowcount:
        return True
    else:
        return False


run(host='localhost', port=8099)