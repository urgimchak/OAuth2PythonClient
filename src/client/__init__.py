from urllib import parse
from flask import Flask, abort, request
import requests
import requests.auth
import urllib
app = Flask(__name__)

CLIENT_ID = "urgimchak"
CLIENT_SECRET = "password"
REDIRECT_URI = "http://localhost:65010/client"


@app.route('/')
def homepage():
    text = '<a href="%s">Authenticate with intex</a>'
    return text % make_authorization_url()


def make_authorization_url():
    from uuid import uuid4
    state = str(uuid4())
    save_created_state(state)
    params = {"client_id": CLIENT_ID, "response_type": "code", "state": state,
              "redirect_uri": REDIRECT_URI, "duration": "temporary"}
    url = "http://localhost:8081/oauth/authorize?" + urllib.parse.urlencode(params)
    return url


@app.route('/client')
def my_callback():
    error = request.args.get('error', '')
    if error:
        return "Error: " + error
    state = request.args.get('state', '')
    if not is_valid_state(state):
        abort(403)
    code = request.args.get('code')
    return "got an access token! %s" % get_token(code)


def get_token(code):
    client_auth = requests.auth.HTTPBasicAuth(CLIENT_ID, CLIENT_SECRET)
    post_data = {"grant_type": "authorization_code", "code": code, "redirect_uri": REDIRECT_URI}
    response = requests.post("http://localhost:8081/oauth/token", auth=client_auth, data=post_data)
    token_json = response.json()
    return token_json["access_token"]


def get_username(access_token):
    headers = {"Authorization": "bearer " + access_token}
    response = requests.get("http://localhost:8082/api", headers=headers)
    me_json = response.json()
    return me_json['authorities']


def save_created_state(state):
    pass


def is_valid_state(state):
    return True


if __name__ == '__main__':
    app.run(debug=True, port=65010)
